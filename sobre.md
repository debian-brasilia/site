---
layout: page
title: Sobre
permalink: /sobre/
---


Comunidade de usuários Debian que buscam aprender e ensinar sobre o Sistema Operacional. Formada por um grupo de amigos que usam e contribuem com o projeto Debian.


A comunidade foi criada em 21 de maio de 2017 em uma reunião de amigos e tem como propósito:

Ensinar Debian a usuários leigos;
Difundir o projeto Debian;
Trabalho voluntário, sem fins lucrativos;
Reunir amigos e voluntários para confraternizar e usar o Debian; e
Ajudar e facilitar a utilização do Debian por usuários domésticos.


<p style="margin-top:550px"></p>