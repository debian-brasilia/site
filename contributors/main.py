import psycopg2
import yaml

contributors = [
    'Arthur Diniz',
    'Francisco Emanoel',
    'Joao Paulo Lima de Oliveira',
    'Sergio de Almeida Cipriano Junior',
    'Ricardo Brandao',
    'Gabriela Pivetta',
    'Carlos Henrique Lima Melara',
    'Thais Reboucas Araujo',
    'Aquila Macedo Costa',
    'Leandro Cunha',
    'Vitor Feij% Leonardo'
]


try:
    connection = psycopg2.connect(
        user="udd-mirror",
        password="udd-mirror",
        host="udd-mirror.debian.net",
        port="5432",
        database="udd"
    )

    cursor = connection.cursor()

    total_uploads_count = {'unstable': 0, 'experimental': 0}

    for contributor in contributors:

        with open(f"../_data/contrib/{contributor.lower().replace(' ', '_')}.yaml", 'w') as out:
            postgreSQL_select_Query = f"SELECT source,version,date,distribution,signed_by_name FROM public.upload_history WHERE changed_by_name LIKE '%{contributor}%' ORDER BY date;"

            cursor.execute(postgreSQL_select_Query)
            print(f'Selecting {contributor} rows from table using cursor.fetchall')
            records = cursor.fetchall()

            data = {
                'name': contributor,
                'uploads': []
            }
            uploads_count = {'unstable': 0, 'experimental': 0}
            for row in records:
                contribution_data = {
                    'package': row[0],
                    'tracker': f'https://tracker.debian.org/pkg/{row[0]}',
                    'version': row[1],
                    'date': f'{str(row[2]).split()[0]}',
                    'distribution': row[3],
                    'sponsor': row[4]
                }
                data['uploads'].append(contribution_data)
                uploads_count[row[3]] += 1
                total_uploads_count[row[3]] += 1

            data['count'] = uploads_count

            yaml.dump(data, out, sort_keys=False)


    with open(f"../_data/contrib.yaml", 'w') as f:
        data = {
            'uploads': total_uploads_count
        }
        yaml.dump(data, f, sort_keys=False)


except (Exception, psycopg2.Error) as error:
    print("Error while fetching data from PostgreSQL", error)

finally:
    # closing database connection.
    if connection:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
