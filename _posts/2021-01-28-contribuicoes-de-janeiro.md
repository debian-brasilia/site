---
layout: post
title:  "Contribuições do mês de Janeiro"
date:   2021-01-28 00:00:00 -0300
description: Iniciando a documentação das nossas atividades
categories: CONTRIBUICAO
author: Admin
color: default
---

No final de 2020, iniciamos um grupo de estudos de empacotamento Debian com o
objetivo de aprender e compartilhar conhecimento. Retomamos as atividades no dia
7 de janeiro de 2021 com encontros semanais todas as quintas às 20 horas.

Ao longo do mês foram realizadas 20 contribuições com a participação de 9 pessoas
ao decorrer dos 4 encontros.

Veja abaixo a lista de contribuições.

#### Arthur Diniz

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1213773/accepted-hey-014-1-source-into-unstable/">hey</a></td>
            <td>0.1.4-1</td>
            <td>2021-01-08</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td><a href="https://tracker.debian.org/news/1219902/accepted-colored-142-1-source-into-unstable/">colored</a></td>
            <td>1.4.2-1</td>
            <td>2021-01-14</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td><a href="https://tracker.debian.org/news/1219909/accepted-gt5-15020111220bzr29-4-source-into-unstable/">gt5</a></td>
            <td>1.5.0~20111220+bzr29-4</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">4</td>
            <td><a href="https://tracker.debian.org/news/1222701/accepted-check-manifest-046-1-source-into-unstable/">check-manifest</a></td>
            <td>0.46-1</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">5</td>
            <td><a href="https://tracker.debian.org/news/1222703/accepted-golang-github-evanphx-json-patch-520-1-source-into-unstable/">golang-github-evanphx-json-patch</a></td>
            <td>5.2.0-1</td>
            <td>2021-01-22</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">6</td>
            <td><a href="https://tracker.debian.org/news/1225233/accepted-golang-github-go-logr-logr-040-1-source-into-unstable/">golang-github-go-logr-logr</a></td>
            <td>0.4.0-1</td>
            <td>2021-01-28</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">7</td>
            <td><a href="https://tracker.debian.org/news/1225237/accepted-golang-github-google-uuid-115-1-source-into-unstable/">golang-github-google-uuid</a></td>
            <td>1.1.5-1</td>
            <td>2021-01-28</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">8</td>
            <td><a href="https://tracker.debian.org/news/1225246/accepted-golang-github-google-uuid-120-1-source-into-unstable/">golang-github-google-uuid</a></td>
            <td>1.2.0-1</td>
            <td>2021-01-28</td>
            <td>Sergio Durigan</td>
        </tr>
    </tbody>
</table>

#### Francisco Ferreira

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1213765/accepted-ruby-csv-319-1-source-into-unstable/">ruby-csv</a></td>
            <td>3.1.9-1</td>
            <td>2021-01-08</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td><a href="https://tracker.debian.org/news/1219906/accepted-ruby-aws-eventstream-110-1-source-into-unstable/">ruby-aws-eventstream</a></td>
            <td>1.1.0-1</td>
            <td>2021-01-15</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td><a href="https://tracker.debian.org/news/1222698/accepted-ruby-oj-3110-1-source-into-unstable/">ruby-oj</a></td>
            <td>3.11.0-1</td>
            <td>2021-01-22</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">4</td>
            <td><a href="https://tracker.debian.org/news/1222699/accepted-ruby-backbone-on-rails-140dfsg-1-source-into-unstable/">ruby-backbone-on-rails</a></td>
            <td>1.4.0+dfsg-1</td>
            <td>2021-01-22</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">5</td>
            <td><a href="https://tracker.debian.org/news/1222702/accepted-ruby-data-migrate-660-1-source-into-unstable/">ruby-data-migrate</a></td>
            <td>6.6.0-1</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
    </tbody>
</table>

#### J.Paulo Oliveira

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1222993/accepted-golang-github-henvic-httpretty-006-1-source-all-into-experimental-experimental/">golang-github-henvic-httpretty</a></td>
            <td>0.0.6-1</td>
            <td>2021-01-22</td>
            <td>Marcio de Souza Oliveira</td>
        </tr>
    </tbody>
</table>

#### Sergio Cipriano

<table class="table">
    <thead>
        <tr>
            <th class="text-center">#</th>
            <th>Pacote</th>
            <th>Versão</th>
            <th>Data</th>
            <th>Sponsor</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td class="text-center">1</td>
            <td><a href="https://tracker.debian.org/news/1213764/accepted-ruby-coveralls-0823-2-source-into-unstable/">ruby-coveralls</a></td>
            <td>0.8.23-2</td>
            <td>2021-01-08</td>
            <td>Lucas Kanashiro</td>
        </tr>
        <tr>
            <td class="text-center">2</td>
            <td><a href="https://tracker.debian.org/news/1219907/accepted-ruby-test-unit-339-1-source-into-unstable/">ruby-test-unit</a></td>
            <td>3.3.9-1</td>
            <td>2021-01-15</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">3</td>
            <td><a href="https://tracker.debian.org/news/1222694/accepted-ruby-sequel-5400-1-source-into-unstable/">ruby-sequel</a></td>
            <td>5.40.0-1</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">4</td>
            <td><a href="https://tracker.debian.org/news/1222704/accepted-ruby-jwt-222-1-source-into-unstable/">ruby-jwt</a></td>
            <td>2.2.2-1</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">5</td>
            <td><a href="https://tracker.debian.org/news/1222705/accepted-ruby-ole-12122-1-source-into-unstable/">ruby-ole</a></td>
            <td>1.2.12.2-1</td>
            <td>2021-01-22</td>
            <td>Sergio Durigan</td>
        </tr>
        <tr>
            <td class="text-center">6</td>
            <td><a href="https://tracker.debian.org/news/1223750/accepted-ruby-ronn-091-2-source-into-unstable/">ruby-ronn</a></td>
            <td>0.9.1-2</td>
            <td>2021-01-25</td>
            <td>Sergio Durigan</td>
        </tr>
    </tbody>
</table>

<p style="margin-top:550px"></p>
