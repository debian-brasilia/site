---
layout: post
title:  NMCLI – NETWORKMANAGER para linha de comando
date:   2018-07-02 00:00:00 -0300
description: Acredito que quase todo usuário GNU/Linux já utilizou ou utiliza o daemon NetworkManager(Framework de gerência de redes).
categories: DICAS NMCLI
author: Marcio De Souza Oliveira
icon: wifi
color: default
---

Olá a todos!

Acredito que quase todo usuário GNU/Linux já utilizou ou utiliza o daemon NetworkManager(Framework de gerência de redes). Para os mais leigos é aquele ícone onde verificamos as conexões de rede, onde escolhemos a rede Wifi por exemplo, em quase todas as distribuições GNU/Linux é o NetworkManager que faz esse papel.

![](https://gitlab.com/debian-brasilia/site/raw/main/_posts/images/2018-07-18/nmcli-1.png)

Ícone do NetworkManager no GNOME

A dica de hoje é ferramenta nmcli que faz parte do NetworkManager, o nmcli é a ferramenta de linha de comando para controlar o NetworkManager. Mas a dica consiste no uso mais simples possível do nmcli, ou seja, apenas executá-lo sem nenhum parâmetro no terminal. Na figura abaixo eu executei o nmcli no meu notebook, só removi os endereços MAC do print :), como podem ver, o comando mostrou o dump das minhas configurações de rede, no meu caso estou conectado na rede wifi Wannacry e tenho várias interfaces de rede por conta do Virtualbox e do VMWare.
```bash
$ nmcli
```

![](https://gitlab.com/debian-brasilia/site/blob/main/_posts/images/2018-07-18/nmcli-2.jpeg)
nmcli saída padrão

Sugiro que teste também os comandos “nmcli device show” e “nmcli connection show”.

Bem gente essa foi a dica rápida de hoje!