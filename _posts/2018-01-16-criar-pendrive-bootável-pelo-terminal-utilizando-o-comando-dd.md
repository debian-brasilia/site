---
layout: post
title:  Criar pendrive bootável pelo terminal utilizando o comando DD
date:   2018-01-16 00:00:00 -0300
description: Vamos criar um pendrive bootável utilizando o comando dd através do terminal. Antes é preciso entender a sintaxe do comando dd.
categories: terminal pendrive boot bootavel
author:  Daniele Adriana Goulart
icon: usb
color: default
---

Vamos criar um pendrive bootável utilizando o comando dd através do terminal. Antes é preciso entender a sintaxe do comando dd:

#dd if=<arquivo de entrada> of= <dipositivo de saída> bs=4MB onde:

- if (input file): o arquivo que você deseja copiar, neste caso será a imagem iso

- of (output file): dispositivo no qual a imagem será copiada. No nosso exemplo será a unidade flash USB

-  bs: Indica o tamanho do bloco que será lido/escrito por vez. Por padrão, o dd trabalha com blocos de 512 bytes. Você pode aumentar este valor tornando o processo mais rápido

1) O primeiro passo é identificar o dispositivo de saída, que no nosso exemplo será a unidade flash USB. Para isso utilize o comando  fdisk -l conforme a tela abaixo:

![](https://gitlab.com/debian-brasilia/site/raw/main/_posts/images/2018-02-06/tela1.png)

Meu pendrive possui 8GB e como pode ser observado na figura, o mesmo foi reconhecido como /dev/sdc.

Uma outra maneira de identificar os dispositivos é através do comando  lsblk:

![](https://gitlab.com/debian-brasilia/site/raw/main/_posts/images/2018-02-06/tela2.png)


É muito importante que você tenha identificado corretamente o dispositivo de saída, pois os dados do dispositivo informado serão apagados. Sendo assim, muito cuidado para não apagar dados do seu disco rígido. Lembre-se de utilizar um pendrive vazio, para evitar perda de dados.

2) Fazer a cópia da imagem iso para o pendrive:

A imagem iso que desejo copiar está na pasta Downloads, portanto devo ir até a pasta indicada e a partir disso realizar a cópia da imagem para o meu Pendrive. Para isso executo os seguintes comandos:

#cd /home/daniele/Downloads

#dd if=debian-9.3.0-amd64-netinst.iso of=/dev/sdc bs=4MB status=progress

![](https://gitlab.com/debian-brasilia/site/raw/main/_posts/images/2018-02-06/tela3.png)

Dica: Por padrão, o dd não exibe o status da cópia sendo realizada, mas você pode utilizar o parâmetro status=progress e acompanhar a evolução da cópia, conforme ilustra a figura acima.

Prontinho! Seu pendrive bootável já está pronto para ser utilizado.

Obs: Caso você queira limpar o pendrive futuramente, utilize o seguinte comando:

dd if=/dev/zero of=/dev/sdc bs=4M status=progress

 