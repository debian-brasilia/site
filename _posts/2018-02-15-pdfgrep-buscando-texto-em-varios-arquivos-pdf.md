---
layout: post
title:  PDFGREP Buscando texto em vários arquivos PDF
date:   2018-02-18 00:00:00 -0300
description: Um amigo no trabalho estava precisando buscar(“grepar“) em vários arquivos PDF para incrementar um script que estava fazendo.
categories: GREPPDF PDF PDFGREP
author: Marcio De Souza Oliveira
icon: file-pdf-o
color: default
---

Olá a todos!!!

Um amigo no trabalho estava precisando buscar(“grepar“) em vários arquivos PDF para incrementar um script que estava fazendo. Pasmem, não é que ele achou o pdfgrep, isso mesmo uma versão de grep para arquivos PDF com suporte a expressões regulares PERL ao alcance de um APT-GET.

1. Instalando o pdfgrep:
Primeiramente atualize sua lista de pacotes com o comando apt update  em seguida você deve instalar o pacote.
```bash
apt update
apt install pdfgrep
```

2. Exemplo de utilização:

Bem eu criei três arquivos PDF respectivamente bomdia.pdf, boatarde.pdf e boanoite.pdf. No exemplo abaixo vou buscar pela palavra “boa”, ignorando maiúsculas e minúsculas, em todos os arquivos pdf no diretório ~/tmp/pdf.
```bash
$ pdfgrep -i boa *pdf
```

![](https://gitlab.com/debian-brasilia/site/blob/main/_posts/images/2018-02-15/pdfgrep.png)
