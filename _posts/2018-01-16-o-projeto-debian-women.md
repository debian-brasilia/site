---
layout: post
title:  "O projeto Debian women"
date:   2018-01-16 00:00:00 -0300
description: Se você está interessada em juntar-se ao Projeto Debian...
categories: WOMEN
author: Admin
icon: female
color: default
---

Se você está interessada em juntar-se ao Projeto Debian, existem muitas coisas que você pode fazer: você pode ser uma desenvolvedora, mantenedora, documentadora ou tradutora Debian, ou nos ajudar testando e relatando bugs.

Quer saber mais… https://www.debian.org/women/