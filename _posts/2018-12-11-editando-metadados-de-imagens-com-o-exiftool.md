---
layout: post
title:  "Editando metadados de imagens com o exiftool"
date:   2018-12-11 00:00:00 -0300
description: Hoje vou mostrar como editar os metadados de um arquivo imagem rapidamente utilizando a ferramenta exiftool.
categories: dicas
author: Marcio De Souza Oliveira
icon: image
color: default
---


Olá a todos,

Hoje vou mostrar como editar os metadados de um arquivo imagem rapidamente utilizando a ferramenta exiftool. Mas o que são metadados mesmo? Na definição mais pura, podemos dizer que metadados são dados sobre outros dados. E onde eles entram nos nossos arquivos de Imagem? Os arquivos de imagem tem vários tipos de metadados, como por exemplo a câmera que foi utilizada para capturar a Imagem, ou o software que foi utilizado, dados de GPS, entre outros.

Como exemplo vou pegar uma foto da árvore de natal do Rio de Janeiro do link [https://fotospublicas.com/arvore-de-natal-do-rio-de-janeiro-na-lagoa/](https://fotospublicas.com/arvore-de-natal-do-rio-de-janeiro-na-lagoa/).

Eu já tenho o exiftool instalado, mas caso você ainda não tenha pode instalar usando o APT:
```bash
apt-get install exiftool
```
Para ler os metadados da imagem basta executar o comando exiftool apontando para o arquivo de imagem:
```bash
$ exiftool rioarv.jpg
```

Metadados da foto rioarv.jpg mostrados com exiftool
A figura acima tem vários metadados como por exemplo a câmera que foi utilizada para tirar a foto, veja o campo “Camera Model Name” : “Canon EOS 7D Mark II”.

Bem vamos a dica, vamos editar o valor do campo “Camera Model Name” para “Maquina do futuro” a título de exemplo:
```bash
$ exiftool -m Model="Maquina do futuro" rioarv.jpg
```

#### Atualizando o campo “Model”

Verificando o novo modelo da nossa câmera
Como podemos ver é muito fácil modificar os metadados de uma imagem, portanto eles não são muito confiáveis, imaginem que eu tirei uma foto do meu celular e ele inseriu a localização, coordenada geográfica da minha posição, o que normalmente acontece se você tiver com o GPS ligado, eu poderia facilmente modificar a informação do campo de GPS e colocar as coordenadas geográficas do Alaska :).

Por último deixo um conselho, se possível desativem a opção de salvar a localização da câmera de seus smartphones, imaginem que vocês tiraram uma foto dentro da sua casa, ou seja, com a localização da sua casa, caso seu celular seja extraviado, quem tiver acesso a suas fotos vai saber exatamente seu endereço.

Por hoje é só, abraço a todos!